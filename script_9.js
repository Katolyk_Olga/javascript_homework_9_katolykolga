// Теоретичні питання
// 1. Опишіть, як можна створити новий HTML тег на сторінці.
// За допомогою скрипта ми можемо створити document.createElement("tag"), де в дужках вказуємо який ми тег створюємо.
// Потім за необхідності ми наповнюємо тег контентом: змінна/тег.innerHTML, або innerText, або textContent.
// Потім вказуємо потрібне місце в документі, куди хочемо розмістити тег, до, після чи в середині потрібних інши елементів.

// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// За допомогою метода insertAdjacentHTML ми можемо вставити HTML в будь-яке місце документа.
// Перший обов'язковий параметр метода, це where, що означає вказівник куди саме ми встявляємо наш html.
// Є наступні варіанти: beforeBegin – вставить перед elem, afterBegin – після початку elem, зсередини, тобто на початок,
// beforeEnd – перед закінченням зсередини elem, тобто в кінець, afterEnd – після elem, ззовні.

// 3. Як можна видалити елемент зі сторінки?
// Видалити елемент зі сторінки можна наступним шляхом: 
// спочатку знайти елемент за допомогою getElemenet.. або querySelector..,
// потім використувати метод elem.remove().

// Завдання
// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. 
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// + Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, 
// до якого буде прикріплений список (по дефолту має бути document.body.
// + Кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
// Приклади масивів, які можна виводити на екран:
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];
// Можна взяти будь-який інший масив.

// Необов'язкове завдання підвищеної складності:
// + Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, 
// + виводити його як вкладений список. Приклад такого масиву:
// + ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
// + Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
// - Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.


let newArrayList = function (paramArray, paramPlace = document.body) {
     // якщо другий параметр paramPlace не задавати, то по замовчуванню вставить в батьківський елемент body

    let newUl = document.createElement("ul");
    paramPlace.append(newUl);

    paramArray.forEach(function(item){

        if (Array.isArray(item)) {
            let newUl2 = document.createElement("ul");             
            newUl.append(newUl2);

            item.forEach(function(item2) {
                let newli2 = document.createElement("li");
                newli2.innerText = item2;
                newUl2.append(newli2);
            });   

        }else{
            let newli = document.createElement("li");
            newli.innerText = item;
            // newli.textContent = item;
            newUl.append(newli);
       }      
    });    
};

let newParamPlace = document.createElement("div");
newParamPlace.innerText = "Це мій список:";
document.body.prepend(newParamPlace);

newArrayList(["hello", "world", "Kiev", ["Borispol", "Irpin"], "Kharkiv", "Odessa", "Lviv"], newParamPlace);

console.dir(document.body.children);           //дивимось дочірні елементи нашого body, чому там два скрипта?
console.log(document.querySelector("ul"));        //дивимось наш список
console.log(document.querySelector("ul").children);  //дивимось дочірні елементи нашого списку
console.log(document.querySelector("ul").parentElement) //дивимось батьківскьйи елемент нашого списку




// Перший варіант, з простим масивом:
//
// let newArrayList = function (paramArray) {
//     let newUl = document.createElement("ul");
//     document.body.prepend(newUl);
//
//     paramArray.forEach(function(item){
//         let newli = document.createElement("li");
//         newli.innerText = item;
//         newUl.append(newli);
//        });    
//     };
// newArrayList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
// console.dir(document.body.children);
// console.log(document.querySelector("ul"));
// console.log(document.querySelector("ul").children);




// Другий варіант, він все складає в один список:
//
// let newUl = document.createElement("ul");
// document.body.append(newUl);
//
// let newArrayList = function (paramArray) {
//     paramArray.forEach(function (item) {
//
//         if (Array.isArray(item)) {
//             newArrayList(item);
//         } else {
//             let newli = document.createElement("li");
//             newli.innerText = item;
//             newUl.append(newli);    
//         }
//     });
// };
// newArrayList(["hello", "world", "Kiev", ["Borispol", "Irpin"], "Kharkiv", "Odessa", "Lviv"]);
// console.dir(document.body.children);
// console.log(document.querySelector("ul"));
// console.log(document.querySelector("ul").children);


